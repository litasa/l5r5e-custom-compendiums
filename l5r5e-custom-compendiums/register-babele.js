Hooks.once('init', () => {
    if (typeof Babele !== 'undefined') {
        // En
        Babele.get().register({
            module: 'l5r5e-custom-compendiums',
            lang: 'en',
            dir: 'compendiums/en'
        });

        // Fr
        // Babele.get().register({
        //     module: 'l5r5e-custom-compendiums',
        //     lang: 'fr',
        //     dir: 'compendiums/fr'
        // });
    }
});
