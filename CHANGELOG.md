# Changelog
Date format : day/month/year

- 29/05/2023 - 1.0.5 - L5R5e v1.10.0 - FoundryVTT v11 compatibility update.
- 03/09/2022 - 1.0.4 - L5R5e v1.9.0  - Compendium update (FoundryVTT v10 and WotW).
- 09/01/2022 - 1.0.3 - L5R5e v1.5.0  - Compendium update (FoundryVTT v9).
- 19/12/2021 - 1.0.2 - L5R5e v1.4.0  - Compendium update.
- 15/09/2021 - 1.0.1 - L5R5e v1.3.4  - Compendium update.
- 24/06/2021 - 1.0.0 - L5R5e v1.3.3  - First release.
